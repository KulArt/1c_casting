#include "soliter.h"

void Soliter::Read() {
  for (int i = 0; i < heaps_; ++i) {
    for (int j = 0; j < 9; ++j) {
      std::cin >> field_[i][j];
    }
  }
}

bool Soliter::Solve() {
  while(true) {
    if (!is_solvable_) {
      return false;
    }
    Prepare();
    if (field_[0].empty()) {
      return true;
    }
    FreeAce(FindAce());
    MakeRow();
    DeleteRow();
    FillZero();
  }
}

void Soliter::Prepare() {
  if (field_[0].empty()) {
    for (int i = 1; i < heaps_; ++i) {
      if (!field_[i].empty()) {
        std::swap(field_[i], field_[0]);
      }
    }
  }
}

int Soliter::FindAce() {
  int heap_min = 0;
  int pos = 0;
  for (int i = 0; i < heaps_; ++i) {
    for (ssize_t j = field_[i].size() - 1; j >= pos; --j) {
      if (field_[i][j] == 9 && j > pos) {
        pos = j;
        heap_min = i;
      }
    }
  }
  std::swap(field_[0], field_[heap_min]);
  return pos;
}

void Soliter::FreeAce(const int pos) {
  for (ssize_t i = field_[0].size() - 1; i > pos; --i) {
    int card = field_[0][field_[0].size() - 1];
    int place = -1;
    int next_pos = 0;
    for (int j = 1; j < heaps_; ++j) {
      if (field_[j][field_[j].size() - 1] > place && card < field_[j][field_[j].size() - 1]) {
        place = field_[j][field_[j].size() - 1];
        next_pos = j;
      }
    }
    if (card < place) {
      field_[next_pos].push_back(card);
      field_[0].pop_back();
    }
  }
}

void Soliter::FreeCard(int curr_card, int pos) {
  for (int i = field_[1].size() - 1; i > pos; --i) {
    int card = field_[1][field_[1].size() - 1];
    int place = -1;
    int next_pos = 0;
    for (int j = 2; j < heaps_; ++j) {
      if (field_[j][field_[j].size() - 1] > place && card < field_[j][field_[j].size() - 1]) {
        place = field_[j][field_[j].size() - 1];
        next_pos = j;
      }
    }
    if (card < place) {
      field_[next_pos].push_back(card);
      field_[1].pop_back();
    }
  }
}

int Soliter::FindCard(int curr_card) {
  int heap_min = 0;
  int pos = 0;
  for (int i = 1; i < heaps_; ++i) {
    for (ssize_t j = field_[i].size() - 1; j >= pos; --j) {
      if (field_[i][j] > curr_card) {
        break;
      }
      if (field_[i][j] == curr_card && j > pos) {
        pos = j;
        heap_min = i;
      }
    }
  }
  if (heap_min == 0) {
    is_solvable_ = false;
    return -1;
  }
  std::swap(field_[1], field_[heap_min]);
  return pos;
}

void Soliter::MoveCard() {
  field_[0].push_back(field_[1].back());
  field_[1].pop_back();
}

void Soliter::MakeRow() {
  for (int curr_card = 8; curr_card >= 1; --curr_card) {
    FreeCard(curr_card, FindCard(curr_card));
    MoveCard();
  }
}

void Soliter::DeleteRow() {
  for (int i = 0; i < 9; ++i) {
    field_[0].pop_back();
  }
}

void Soliter::FillZero() {
  for (int i = 0; i < heaps_; ++i) {
    if (field_[i].empty()) {
      field_[i].push_back(0);
    }
  }
}