#ifndef INC_1C_SOLITER
#define INC_1C_SOLITER

#include <vector>
#include <iostream>

class Soliter {
 public:
  void Read();
  bool Solve();

 private:
  static const int heaps_ = 8;
  std::vector<std::vector<int>> field_ = std::vector<std::vector<int>>(heaps_, std::vector<int>(heaps_ + 1, 0));
  bool is_solvable_ = true;

  void Prepare();
  int FindAce();
  void FreeAce(const int pos);
  void FreeCard(int curr_card, int pos);
  int FindCard(int curr_card);
  void MoveCard();
  void MakeRow();
  void DeleteRow();
  void FillZero();
};

#endif
